<?php


namespace JaPhIM\lib\core;


use JaPhIM\lib\log\Log;
use JaPhIM\lib\log\LogHandler;

class Core implements HistoryAbstract
{

    protected $db;

    protected $log;

    public function __construct($db)
    {
        $this->db = $db;
        $logHandle = new LogHandler();
        $this->log = Log::Init($logHandle,15);
    }

    public function online($user_id)
    {
        $db = $this->db->get();
        $user = $db->select('user','*',[
            'user_id' => $user_id
        ]);

        if (empty($user)){
            $db->insert('user',[
                'user_id' => $user_id,
                'is_online' => '1'
            ]);
        }else{
            $db->update('user',[
                'is_online' => '1'
            ],[
                'user_id' => $user_id
            ]);
        }
        $this->log::INFO('SQL:'.print_r($db->log()));
        $this->log::ERROR('SQL:'.print_r($db->error()));
        $this->db->put($db);
    }

    public function offline($user_id)
    {
        $db = $this->db->get();
        $db->update('user',[
            'is_online' => '0'
        ],[
            'user_id' => $user_id
        ]);
        $this->log::INFO('SQL:'.print_r($db->log()));
        $this->log::ERROR('SQL:'.print_r($db->error()));
        $this->db->put($db);
    }

    public function getHistory($user_id, $another_id)
    {
        $db = $this->db->get();
        $user_main_id = $db->select('main','id',[
            'AND' => [
                'user_id' => $user_id,
                'another_id' => $another_id
            ],
        ]);
        $user_chat = $db->select('content',['content','time'],[
            'AND' => [
                'main_id' => $user_main_id,
                'user_id' => $user_id
            ],
            'ORDER' =>[
                'id' => 'DESC'
            ],
            'LIMIT' => [0,10]
        ]);
        $another_main_id = $db->select('main','id',[
            'AND' => [
                'user_id' => $another_id,
                'another_id' => $user_id
            ]
        ]);
        $another_chat = $db->select('content',['content','time'],[
            'AND' =>[
                'main_id' => $another_main_id,
                'user_id' => $another_id
            ],
            'ORDER' =>[
                'id' => 'DESC'
            ],
            'LIMIT' => [0,10]
        ]);
        $this->log::INFO('SQL:'.print_r($db->log()));
        $this->log::ERROR('SQL:'.print_r($db->error()));
        $this->db->put($db);
        $data = [
            'user_chat' => $user_chat,
            'another_chat' => $another_chat
        ];
        return $data;
    }

    public function addHistory($user_id, $another_id, $msg)
    {
        $db = $this->db->get();
        $main_id = $db->select('main','id',[
            'AND' => [
                'user_id' => $user_id,
                'another_id' => $another_id
            ]
        ]);
        $db->insert('content',[
            'main_id' => $main_id,
            'user_id' => $another_id,
            'content' => $msg,
            'time'    => time()
        ]);
        $this->log::INFO('SQL:'.print_r($db->log()));
        $this->log::ERROR('SQL:'.print_r($db->error()));
        $this->db->put($db);
        return true;
    }

    public function getUserInfo($user_id)
    {
        $db = $this->db->get();
        $user =  $db->get('user','*',[
            'user_id' => $user_id
        ]);
        $this->db->put($db);
        return $user;
    }

    public function joinFriend($user_id,$another_id)
    {
        $db = $this->db->get();
        $conn1 = $db->insert('main',[
            'user_id' => $user_id,
            'another_id' => $another_id
        ]);

        $conn2 = $db->insert('main',[
            'user_id' => $another_id,
            'another_id' => $user_id
        ]);
        $this->db->put($db);
        return $conn1&&$conn2;
    }

    public function getFriendList($user_id)
    {
        $db = $this->db->get();
        $friendlist = $db->select('main',[
            '[><]user' => ['another_id'=>'user_id']
        ],[
            'user.user_id',
            'user.is_online',
            'main.unread'
        ],[
            'main.user_id' => $user_id
        ]);
        $this->log::INFO('SQL:'.print_r($db->log()));
        $this->log::ERROR('SQL:'.print_r($db->error()));
        return $friendlist;
    }

    public function getOnlineUsers()
    {

    }

    public function getUsers($users)
    {

    }
}