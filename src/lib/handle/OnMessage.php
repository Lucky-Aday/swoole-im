<?php
namespace JaPhIM\lib\handle;

trait OnMessage
{
    private function online($server,  $fd, $msg)
    {
        $this->bind($fd,$msg['user_id']);
        $this->cahce->set($msg['user_id'],$fd);
        $this->core->online($msg['user_id']);
        $this->sendJson($fd,[
            'type' => 'friendlist',
            'code' => 1,
            'content' => $this->core->getFriendList($msg['user_id']),
        ]);
        $this->broadcast([
            'type' => 'online',
            'user_id' => $msg['user_id']
        ]);

        \Swoole\Timer::tick(3000,function (int $timer_id, $user_id) use ($server,$fd){
            if (!$this->isEstablished($fd)){
                $this->core->offline($user_id);
                $this->broadcast([
                    'type' => 'offline',
                    'user_id' => $user_id,
                ]);
                \Swoole\Timer::clear($timer_id);
            }
        },$msg['user_id']);
    }

    private function joinfriend($server,  $fd, $msg)
    {
        $user_id = $msg['user_id'];
        if ($user_id == $msg['friend_id']){
            $data = [
                'type' => 'joinfriend',
                'code' => 0,
                'content' => '请勿加自己为好友'
            ];
            $this->sendJson($fd,$data);
            return false;
        }
        $friendInfo = $this->core->getUserInfo($msg['friend_id']);
        if (!$friendInfo){
            $data = [
                'type' => 'joinfriend',
                'code' => 0,
                'content' => '不存在的好友ID'
            ];
            $this->sendJson($fd,$data);
            return false;
        }
        $res = false;
        $db = $this->db->get();
        $list = $db->select('main','*',[
           'AND'=>[
               'user_id' => $user_id,
               'another_id' => $msg['friend_id']
           ]
        ]);
        $this->db->put($db);
        if (!$list){
            $res = $this->core->joinFriend($user_id,$msg['friend_id']);
        }

        if ($res){
            $data = [
                'type' => 'joinfriend',
                'code' => 1,
                'content' => $friendInfo
            ];
            $this->sendJson($fd,$data);
            $friend_fd = $this->cahce->get($msg['friend_id']);
            $data = [
                'type' => 'joinfriend',
                'code' => 1,
                'content' => $this->core->getUserInfo($user_id)
            ];
            $this->sendJson($friend_fd,$data);
        }else{
            $data = [
                'type' => 'joinfriend',
                'code' => 0,
                'content' => '已经是好友请勿重试'
            ];
            $this->sendJson($fd,$data);
        }
    }

    public function sendchat($server,  $fd, $msg)
    {
        $another_fd = $this->cahce->get($msg['another_id']);
        $data = [
            'type' => 'chat',
            'user_id' => $msg['user_id'],
            'msg' => $msg['msg']
        ];
        $this->sendJson($another_fd,$data);
        $this->core->addHistory($msg['user_id'],$msg['another_id'],$msg['msg']);
    }

}