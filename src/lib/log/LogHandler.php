<?php


namespace JaPhIM\lib\log;


class LogHandler implements ILogHandler
{
    private $handle = null;

    protected $config;

    public function __construct()
    {
        $this->config = require_once __DIR__.'/../../../config.php';
        $this->handle = fopen($this->config['file_log']['log_dir'].date('Y-m-d').'.log','a');
    }

    public function write($msg)
    {
        fwrite($this->handle, $msg, 4096);
    }

    public function __destruct()
    {
        fclose($this->handle);
    }
}