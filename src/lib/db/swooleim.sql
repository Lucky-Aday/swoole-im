/*
Navicat MySQL Data Transfer

Source Server         : my
Source Server Version : 50730
Source Host           : 39.108.237.19:3306
Source Database       : swooleim

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-09-01 15:36:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for swooleim_content
-- ----------------------------
DROP TABLE IF EXISTS `swooleim_content`;
CREATE TABLE `swooleim_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `main_id` int(10) unsigned NOT NULL COMMENT '主表id',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `content` text COLLATE utf8_bin NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for swooleim_group
-- ----------------------------
DROP TABLE IF EXISTS `swooleim_group`;
CREATE TABLE `swooleim_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_list` text COLLATE utf8_bin NOT NULL COMMENT '群用户id列表使用","分割',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for swooleim_list
-- ----------------------------
DROP TABLE IF EXISTS `swooleim_list`;
CREATE TABLE `swooleim_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `main_id` int(10) unsigned NOT NULL COMMENT '主表id',
  `user_id` int(10) unsigned NOT NULL COMMENT '发起方用户id',
  `another_id` int(10) unsigned NOT NULL COMMENT '接收方用户id',
  `unread` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000' COMMENT '未读数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for swooleim_main
-- ----------------------------
DROP TABLE IF EXISTS `swooleim_main`;
CREATE TABLE `swooleim_main` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '发起方用户id',
  `another_id` int(10) unsigned DEFAULT NULL COMMENT '接收方用户id',
  `group_id` int(11) DEFAULT NULL COMMENT '群聊id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for swooleim_user
-- ----------------------------
DROP TABLE IF EXISTS `swooleim_user`;
CREATE TABLE `swooleim_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `is_online` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '是否在线 0:不在线，1:在线',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
