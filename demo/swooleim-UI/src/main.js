import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueRouter from 'vue-router'
import Index from './components/Index.vue'
import App from './App.vue'

Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.use(VueRouter);

const routers = new VueRouter({
	mode:'history',
	routes: [
			{
					path: '/Index',
					name: 'Index', //在后期使用这个配置好的路由时，需要用到name属性
					component: Index
			},
	]
})

new Vue({
  render: h => h(App),
	routers
}).$mount('#app')
